# UBML SDK
![UBML SDK](ubml_logo.png) 

UBML低代码建模工具 

[![license](https://img.shields.io/github/license/seata/seata.svg)](https://www.apache.org/licenses/LICENSE-2.0.html)

## 简介

工程化是UBML低代码建模的一大特点，UBML将模型视为源码，模型可存储为文件，以工程化结构组织，并提供SDK，可以与流行的源代码管理工具、CI/CD工具进行集成，融入现代化的开发模式。
UBML SDK提供cli和IDE两种使用形式：
cli主要用于CI脚本中，IDE（Web）则提供图形化工具入口，cli和IDE复用一套公共类库，二者仅提供不同的入口封装。IDE（Web）模式支持多人协同开发，SDK的类库会加载到Server进程中，需要考虑多人并发的支持。

![UBML SDK 概览](design/overview/ubml-sdk-overviewv1.png#pic_center)