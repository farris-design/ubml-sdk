/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.vo.definition.action;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.io.IOException;
import java.io.Serializable;
import org.openatom.ubml.model.vo.definition.action.mappedcdp.MappedCdpActionParameterCollection;
import org.openatom.ubml.model.vo.definition.json.operation.MappedCdpActionDeserializer;
import org.openatom.ubml.model.vo.definition.json.operation.MappedCdpActionSerializer;

/**
 * The Definition Of The Parameter With Component
 *
 * @ClassName: MappedCdpAction
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
@JsonSerialize(using = MappedCdpActionSerializer.class)
@JsonDeserialize(using = MappedCdpActionDeserializer.class)
public class MappedCdpAction extends MappedCdpActionBase implements Cloneable, Serializable {

    private MappedCdpActionParameterCollection mappedCdpActionParams;

    /**
     * 类型
     */
    @Override
    public ViewModelActionType getType() {
        return ViewModelActionType.VMAction;
    }

    public MappedCdpAction() {
        mappedCdpActionParams = new MappedCdpActionParameterCollection();
    }

    /**
     * 克隆
     *
     * @return VO节点映射
     */
    @Override
    public MappedCdpAction clone() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.readValue(objectMapper.writeValueAsString(this), MappedCdpAction.class);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected IViewModelParameterCollection getParameters() {
        return mappedCdpActionParams;
    }
}