/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.vo.definition.json.model;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import java.io.IOException;
import org.openatom.ubml.model.common.definition.cef.entity.CustomizationInfo;
import org.openatom.ubml.model.common.definition.cef.json.CefNames;
import org.openatom.ubml.model.common.definition.cef.json.SerializerUtils;
import org.openatom.ubml.model.vo.definition.common.ValueHelpConfig;
import org.openatom.ubml.model.vo.definition.json.ViewModelJsonConst;
import org.openatom.ubml.model.vo.definition.json.operation.VmActionCollectionDeserializer;

/**
 * The Josn Deserializer Of View Model Help Configuration
 *
 * @ClassName: ValueHelpConfigDeserizlizer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ValueHelpConfigDeserizlizer extends JsonDeserializer<ValueHelpConfig> {

  @Override
  public ValueHelpConfig deserialize(JsonParser jsonParser, DeserializationContext ctxt) {
    ValueHelpConfig config = new ValueHelpConfig();
    SerializerUtils.readStartObject(jsonParser);
    while (jsonParser.getCurrentToken() == JsonToken.FIELD_NAME) {
      String propName = SerializerUtils.readPropertyName(jsonParser);
      switch (propName) {
        case ViewModelJsonConst.HelperId:
          config.setHelperId(SerializerUtils.readPropertyValue_String(jsonParser));
          break;
        case ViewModelJsonConst.FilterExpression:
          config.setFilterExpression(SerializerUtils.readPropertyValue_String(jsonParser));
          break;
        case ViewModelJsonConst.ElementId:
          config.setElementId(SerializerUtils.readPropertyValue_String(jsonParser));
          break;
        case ViewModelJsonConst.EnableCustomHelpAuth:
          config.setEnableCustomHelpAuth(SerializerUtils.readPropertyValue_boolean(jsonParser));
          break;
        case CefNames.CUSTOMIZATION_INFO:
          config.setCustomizationInfo((CustomizationInfo) SerializerUtils
              .readPropertyValue_Object(CustomizationInfo.class, jsonParser));
          try {
            jsonParser.nextToken();
          } catch (IOException e) {
            throw new RuntimeException(
                String.format("GspCommonDataTypeDeserializer反序列化错误：%1$s", propName));
          }
          break;
        case ViewModelJsonConst.HelpExtend:
          SerializerUtils.readStartObject(jsonParser);
          if (jsonParser.getCurrentToken() == JsonToken.FIELD_NAME) {
            String propertyName = SerializerUtils.readPropertyName(jsonParser);
            switch (propertyName) {
              case ViewModelJsonConst.BeforeHelp:
                VmActionCollectionDeserializer deserializer1 = new VmActionCollectionDeserializer();
                config.getHelpExtend().setBeforeHelp(deserializer1.deserialize(jsonParser, null));
                break;
              default:
                throw new RuntimeException("未定义ValueHelpConfig.HelpExtend属性名" + propertyName);
            }
          }
          SerializerUtils.readEndObject(jsonParser);
          break;
        default:
          throw new RuntimeException("未定义ValueHelpConfig" + propName);
      }
    }
    SerializerUtils.readEndObject(jsonParser);
    return config;
  }
}
