/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
//package org.openatom.ubml.model.vo.definition.common;
//
//import com.inspur.edp.bef.bizentity.GspBusinessEntity;
//import com.inspur.edp.bef.bizentity.beenum.BEOperationType;
//import com.inspur.edp.bef.bizentity.operation.BizMgrAction;
//import com.inspur.edp.bef.bizentity.operation.collection.BizMgrActionCollection;
//import com.inspur.edp.bef.bizentity.operation.componentbase.BizParameter;
//import com.inspur.edp.bef.bizentity.operation.componentbase.BizVoidReturnType;
//import com.inspur.edp.bef.bizentity.operation.componentinterface.IBizParameter;
//import com.inspur.edp.cef.designtime.api.IGspCommonField;
//import com.inspur.edp.cef.designtime.api.collection.GspAssociationCollection;
//import com.inspur.edp.cef.designtime.api.collection.GspAssociationKeyCollection;
//import com.inspur.edp.cef.designtime.api.collection.GspEnumValueCollection;
//import com.inspur.edp.cef.designtime.api.element.GspAssociation;
//import com.inspur.edp.cef.designtime.api.element.GspAssociationKey;
//import com.inspur.edp.cef.designtime.api.element.GspElementObjectType;
//import com.inspur.edp.cef.designtime.api.element.GspEnumValue;
//import com.inspur.edp.cef.designtime.api.util.Guid;
//import com.inspur.edp.das.commonmodel.IGspCommonElement;
//import com.inspur.edp.das.commonmodel.IGspCommonModel;
//import com.inspur.edp.das.commonmodel.IGspCommonObject;
//import com.inspur.edp.das.commonmodel.collection.GspUniqueConstraintCollection;
//import com.inspur.edp.das.commonmodel.entity.GspCommonElement;
//import com.inspur.edp.das.commonmodel.entity.element.ElementCodeRuleConfig;
//import com.inspur.edp.das.commonmodel.entity.object.GspUniqueConstraint;
//import org.openatom.ubml.model.vo.definition.DotNetToJavaStringHelper;
//import org.openatom.ubml.model.vo.definition.GspViewModel;
//import org.openatom.ubml.model.vo.definition.GspViewModelElement;
//import org.openatom.ubml.model.vo.definition.GspViewObject;
//import org.openatom.ubml.model.common.definition.cef.IGspCommonField;
//import org.openatom.ubml.model.common.definition.cef.collection.GspAssociationCollection;
//import org.openatom.ubml.model.common.definition.cef.collection.GspAssociationKeyCollection;
//import org.openatom.ubml.model.common.definition.cef.collection.GspEnumValueCollection;
//import org.openatom.ubml.model.common.definition.cef.element.GspAssociation;
//import org.openatom.ubml.model.common.definition.cef.element.GspAssociationKey;
//import org.openatom.ubml.model.common.definition.cef.element.GspElementObjectType;
//import org.openatom.ubml.model.common.definition.cef.element.GspEnumValue;
//import org.openatom.ubml.model.common.definition.cef.util.Guid;
//import org.openatom.ubml.model.common.definition.commonmodel.IGspCommonElement;
//import org.openatom.ubml.model.common.definition.commonmodel.IGspCommonModel;
//import org.openatom.ubml.model.common.definition.commonmodel.IGspCommonObject;
//import org.openatom.ubml.model.common.definition.commonmodel.collection.GspUniqueConstraintCollection;
//import org.openatom.ubml.model.common.definition.commonmodel.entity.GspCommonElement;
//import org.openatom.ubml.model.common.definition.commonmodel.entity.element.ElementCodeRuleConfig;
//import org.openatom.ubml.model.common.definition.commonmodel.entity.object.GspUniqueConstraint;
//import org.openatom.ubml.model.vo.definition.DotNetToJavaStringHelper;
//import org.openatom.ubml.model.vo.definition.GspViewModel;
//import org.openatom.ubml.model.vo.definition.GspViewModelElement;
//import org.openatom.ubml.model.vo.definition.GspViewObject;
//import org.openatom.ubml.model.vo.definition.action.MappedBizAction;
//import org.openatom.ubml.model.vo.definition.action.mappedbiz.MappedBizActionParameter;
//import org.openatom.ubml.model.vo.definition.action.viewmodelbase.ViewModelParameter;
//import org.openatom.ubml.model.vo.definition.action.viewmodelbase.ViewModelParameter;
//import org.openatom.ubml.model.vo.definition.action.viewmodelbase.ViewModelReturnValue;
//import org.openatom.ubml.model.vo.definition.action.viewmodelbase.ViewModelVoidReturnValue;
//import org.openatom.ubml.model.vo.definition.common.mapping.GspVoElementMapping;
//import org.openatom.ubml.model.vo.definition.common.mapping.GspVoElementSourceType;
//import org.openatom.ubml.model.vo.definition.common.mapping.GspVoObjectMapping;
//import org.openatom.ubml.model.vo.definition.common.mapping.GspVoObjectSourceType;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Map;
//import java.util.stream.Collectors;
//
///**
// * The Tool Of Convert Between Busiess Entity And  View Model
// *
// * @ClassName: ConvertUtils
// * @Author: Benjamin Gong
// * @Date: 2021/1/11 17:13
// * @Version: V1.0
// */
//public final class ConvertUtils {
//    private static String AUTO_GEN_VM_PREFIX = "GenVo";
//    private static String AUTO_GEN_VM_SUFFIX = "Service";
//    private static String errorToken = "#GspBefError# ";
//
//    public static GspViewModel convertToViewModel(GspBusinessEntity be, String pkgName, String metadataId) {
//        return convertToViewModel(be, pkgName, metadataId, "");
//    }
//
//    public static GspViewModel convertToViewModel(GspBusinessEntity be, String pkgName, String metadataId, String voGeneratingAssembly) {
//        //① CM结构构造
//        GspViewModel vm = convertToGspViewModel(be, pkgName, metadataId, voGeneratingAssembly);
//        vm.getMapping().setMapType(MappingType.BizEntity);
//        //② BeMgrAction构造
//        var beMgrActions = be.getBizMgrActions(); //be.GetCustomMgrActions();
//        buildVmActions(beMgrActions, vm, pkgName, metadataId);
//
//        vm.setIsUseNamespaceConfig(true);
//        return vm;
//    }
//
//    private static GspViewModel convertToGspViewModel(IGspCommonModel cm, String pkgName, String metadataId, String voGeneratingAssembly) {
//        GspViewModel vm = new GspViewModel();
//        //① BasicInfo
//        convertBasicInfo(cm, vm, pkgName, metadataId, voGeneratingAssembly);
//        //② MainObject
//        vm.setMainObject(toObject(cm.getMainObject(), pkgName, metadataId, null, GspVoObjectSourceType.BeObject));
//        //③ BelongModel递归赋值
//        vm.getMainObject().setBelongModel(vm);
//        vm.getMainObject().setBelongModelID(vm.getID());
//        setChildObjectBelongModel(vm.getMainObject(), vm);
//        return vm;
//    }
//
//
//    /**
//     * 设置子对象的所属模型属性
//     *
//     * @param parentObject 父对象
//     * @param cm           模型
//     */
//    private static void setChildObjectBelongModel(IGspCommonObject parentObject, IGspCommonModel cm) {
//        if (parentObject == null || parentObject.getContainChildObjects() == null || parentObject.getContainChildObjects().size() == 0) {
//            return;
//        }
//        for (IGspCommonObject childObject : parentObject.getContainChildObjects()) {
//            childObject.setBelongModel(cm);
//            childObject.setBelongModelID(cm.getID());
//            // 递归
//            setChildObjectBelongModel(childObject, cm);
//        }
//    }
//
//    private static void convertBasicInfo(IGspCommonModel cm, GspViewModel vm, String pkgName, String metadataId, String voGeneratingAssembly) {
//        vm.setID(Guid.newGuid().toString());
//        vm.setCode(String.format("%1$s%2$s", cm.getCode(), AUTO_GEN_VM_SUFFIX));
//        vm.setName(String.format("%1$s_%2$s", AUTO_GEN_VM_PREFIX, cm.getName()));
//        vm.setIsVirtual(false); //be带出，IsVirtual=false
//        //vm.PrimayKeyID = cm.PrimayKeyID;
//        //vm.EntityType = cm.ExtendType;
//        vm.setEntityType(cm.getEntityType());
//        // todo:时区，临时屏蔽
////        vm.setEnableStdTimeFormat(true);
//        //vm.ExtendNodeList = cm.ExtendNodeList;
////        if (cm.getFkConstraints() != null && cm.getFkConstraints().size() > 0) {
////            //vm.FkConstraints.addRange(cm.FkConstraints);
////        }
//        if (DotNetToJavaStringHelper.isNullOrEmpty(voGeneratingAssembly)) {
//            vm.setDotnetGeneratingAssembly(cm.getDotnetGeneratingAssembly()+".vo");
//            vm.setGeneratingAssembly(cm.getGeneratingAssembly() + ".vo");
//        } else {
//            vm.setDotnetGeneratingAssembly(voGeneratingAssembly+ ".vo");
//            vm.setGeneratingAssembly(voGeneratingAssembly+ ".vo");
//        }
//        ViewModelMapping tempVar = new ViewModelMapping();
//        tempVar.setMapType(MappingType.BizEntity);
//        tempVar.setTargetMetadataPkgName(pkgName);
//        tempVar.setTargetMetadataId(metadataId);
//        tempVar.setTargetObjId(cm.getID());
//        vm.setMapping(tempVar);
//    }
//
//    //    //Object
//////ORIGINAL LINE: public static GspViewObject toObject(IGspCommonObject cmObject, string pkgName, string metadataId, string parentObjIDElementId, GspVoObjectSourceType sourceType = GspVoObjectSourceType.BeObject)
//    public static GspViewObject toObject(IGspCommonObject cmObject, String pkgName, String metadataId, String parentObjIDElementId, GspVoObjectSourceType sourceType) {
//        GspViewObject vmObject = new GspViewObject();
//
//        //① BasicInfo
//        convertObjectBasicInfo(cmObject, vmObject, pkgName, metadataId, sourceType);
//        //② Element
//        convertContainElements(cmObject, vmObject, pkgName, metadataId, sourceType);
//        //③ 更新字段相关的属性（分级信息、ID字段等）
//        // QO无相关属性，故不需要传入sourceType区分qo源
//        convertObjectSelfInfo(cmObject, vmObject, parentObjIDElementId, sourceType);
//        //⑤ ChildObject
//        // QO无子节点
//        convertChildObjects(cmObject, vmObject, pkgName, metadataId);
//
//        return vmObject;
//    }
//
//    //
////
//    private static void convertObjectBasicInfo(IGspCommonObject cmObject, GspViewObject vmObject, String pkgName, String metadataId, GspVoObjectSourceType sourceType) {
//        vmObject.setID(Guid.newGuid().toString());
//        vmObject.setCode(cmObject.getCode());
//        vmObject.setName(cmObject.getName());
//        vmObject.setObjectType(cmObject.getObjectType());
//        vmObject.setRefObjectName(cmObject.getRefObjectName());
////        vmObject.setLogicDelete(cmObject.getLogicDelete());
//        vmObject.setOrderbyCondition(cmObject.getOrderbyCondition());
//        vmObject.setFilterCondition(cmObject.getFilterCondition());
////        vmObject.setRecordDelData() = cmObject.RecordDelData;
//        vmObject.setIsReadOnly(cmObject.getIsReadOnly());
//        if (cmObject.getKeys() != null && cmObject.getKeys().size() > 0) {
//            for (int i = 0; i < cmObject.getKeys().size(); i++) {
//                vmObject.getKeys().add(cmObject.getKeys().get(i).clone());
//            }
//        }
//        vmObject.setIsRef(false);
//        GspVoObjectMapping tempVar = new GspVoObjectMapping();
//        tempVar.setMapType(MappingType.BEObject);
//        tempVar.setTargetMetadataPkgName(pkgName);
//        tempVar.setTargetMetadataId(metadataId);
//        tempVar.setTargetObjId(cmObject.getID());
//        tempVar.setSourceType(sourceType);
//        vmObject.setMapping(tempVar);
//
//        // 源相关属性
//        convertObjSourceTypeRelatedProp(cmObject, vmObject, sourceType);
//    }
//
//    private static void convertObjSourceTypeRelatedProp(IGspCommonObject cmObj, GspViewObject vmObject, GspVoObjectSourceType sourceType) {
//        switch (sourceType) {
//            case BeObject:
//            case QoObject:
//                vmObject.setIsVirtual(false); //be带出，IsVirtual=false
//                break;
//            case VoObject:
//                vmObject.setIsVirtual(cmObj.getIsVirtual());
//
//                GspViewObject originVo = (GspViewObject) cmObj;
//                vmObject.setIsBeckendOnly(originVo.getIsBeckendOnly());
//                vmObject.setPrimaryKey(originVo.getPrimaryKey()); // 无用
//                vmObject.setDefaultPageSize(originVo.getDefaultPageSize());
//                vmObject.setExtendProperties(originVo.getExtendProperties());
//                break;
//            default:
//                throw new RuntimeException(errorToken + "不存在的GspVoObjectSourceType"+ sourceType.toString() + errorToken);
//        }
//    }
//
//
//    //字段相关属性，需额外处理
//
//    /**
//     * vo对象转换时，更新字段相关属性
//     *
//     * @param cmObject
//     * @param vmObject
//     * @param parentObjIDElementId
//     */
//
////ORIGINAL LINE: private static void convertObjectSelfInfo(IGspCommonObject cmObject, GspViewObject vmObject, string parentObjIDElementId, GspVoObjectSourceType objSourceType = GspVoObjectSourceType.BeObject)
//    private static void convertObjectSelfInfo(IGspCommonObject cmObject, GspViewObject vmObject, String parentObjIDElementId, GspVoObjectSourceType objSourceType) {
//
//        List<IGspCommonField> elementList = vmObject.getContainElements().stream().filter(item -> item.getIsVirtual() == false).collect(Collectors.toList());
//        Map<String, IGspCommonField> viewElements = elementList.stream().collect(Collectors.toMap(IGspCommonField::getID,item -> item));
//        java.util.HashMap<String, String> elementMappings = getElementMappingsDic(elementList);
//
//        //① ID字段和Keys更新
//        updateIDElement(cmObject, vmObject);
//        updateViewObjectKeys(cmObject, vmObject, parentObjIDElementId);
//        //② 分级信息HirachyInfo
////        GspVoElementSourceType eleSourceType = GetEleSourceType(objSourceType);
////        updateHirarchyInfo(cmObject, vmObject, elementMappings, viewElements, eleSourceType);
//        //③ 列生成规则ColumnGenerateID
//        updateColumnGenerateId(cmObject, vmObject, elementMappings);
//        //④ 唯一性约束ContainConstraints
//        updateContainConstraints(cmObject, vmObject, elementMappings, viewElements);
////        //⑤	时间戳(创建人等信息)
////        updateTimeStampElements(cmObject, vmObject, elementMappings);
//        //⑥ 状态字段
//        updateStateElementId(cmObject, vmObject, elementMappings);
//
//    }
//
//    /**
//     * 更新状态字段
//     *
//     * @param beObject
//     * @param viewObj
//     * @param elementMappings
//     */
//    public static void updateStateElementId(IGspCommonObject beObject, GspViewObject viewObj, java.util.HashMap<String, String> elementMappings) {
//        if (!DotNetToJavaStringHelper.isNullOrEmpty(beObject.getStateElementID()) && elementMappings.containsKey(beObject.getStateElementID())) {
//            viewObj.setStateElementID(elementMappings.get(beObject.getStateElementID()));
//        }
//    }
//
//    /**
//     * 更新创建人等信息
//     *
//     * @param beObject
//     * @param viewObj
//     * @param elementMappings
//     */
//    public static void updateTimeStampElements(IGspCommonObject beObject, GspViewObject viewObj, java.util.HashMap<String, String> elementMappings) {
//    }
//
//    /**
//     * 更新唯一性约束
//     *
//     * @param beObject
//     * @param viewObj
//     * @param viewElements
//     * @param elementMappings
//     */
//    public static void updateContainConstraints(IGspCommonObject beObject, GspViewObject viewObj, java.util.HashMap<String, String> elementMappings, Map<String, IGspCommonField> viewElements) {
//
//        GspUniqueConstraintCollection voUniqueCons = viewObj.getContainConstraints().clone();
//        if (viewObj.getContainConstraints() != null && viewObj.getContainConstraints().size() > 0) {
//            viewObj.getContainConstraints().clear();
//        }
//        if (beObject.getContainConstraints() == null || beObject.getContainConstraints().size() == 0) {
//            return;
//        }
//        for (GspUniqueConstraint beConstraint : beObject.getContainConstraints()) {
//            Object tempVar = beConstraint.clone();
//            GspUniqueConstraint viewModelConstraint = (GspUniqueConstraint) ((tempVar instanceof GspUniqueConstraint) ? tempVar : null);
//            viewModelConstraint.getElementList().clear();
//            for (String element : beConstraint.getElementList()) {
//                if (!elementMappings.containsKey(element)) {
//                    continue;
//                }
//                String vmElementId = elementMappings.get(element);
//                if (DotNetToJavaStringHelper.isNullOrEmpty(vmElementId)) {
//                    continue;
//                }
//                if (!viewElements.containsKey(vmElementId)) {
//                    throw new RuntimeException("没有找到ID为【" + vmElementId + "】的字段#GspBefError#");
//                }
//                viewModelConstraint.getElementList().add(vmElementId);
//            }
//            if (viewObj.getContainConstraints() == null) {
//                viewObj.setContainConstraints(new GspUniqueConstraintCollection());
//            }
//            if (voUniqueCons != null && voUniqueCons.size() > 0) {
//                for (GspUniqueConstraint voCon : voUniqueCons) {
//                    if (voCon.getCode().equals(viewModelConstraint.getCode())) {
//                        viewModelConstraint.setI18nResourceInfoPrefix(voCon.getI18nResourceInfoPrefix());
//                    }
//                }
//            }
//            viewObj.getContainConstraints().add(viewModelConstraint);
//        }
//    }
//
//    /**
//     * 更新ID生成规则
//     *
//     * @param beObject
//     * @param viewObj
//     * @param elementMappings
//     */
//    public static void updateColumnGenerateId(IGspCommonObject beObject, GspViewObject viewObj, java.util.HashMap<String, String> elementMappings) {
//        if (elementMappings.containsKey(beObject.getColumnGenerateID().getElementID())) {
//            viewObj.getColumnGenerateID().setElementID(elementMappings.get(beObject.getColumnGenerateID().getElementID()));
//        }
//        viewObj.getColumnGenerateID().setGenerateType(beObject.getColumnGenerateID().getGenerateType());
//    }
//
//
//    /**
//     * 字段Mapping字典
//     *
//     * @param elementList
//     * @return
//     */
//    public static java.util.HashMap<String, String> getElementMappingsDic(List<IGspCommonField> elementList) {
//        java.util.HashMap<String, String> elementMappings = new java.util.HashMap<String, String>();
//        for (IGspCommonField item : elementList) {
//            GspViewModelElement viewModelElement = (GspViewModelElement) item;
//            if (viewModelElement != null && viewModelElement.getMapping() != null) {
//                String key = ((GspViewModelElement) ((item instanceof GspViewModelElement) ? item : null)).getMapping().getTargetObjId();
//                String value = item.getID();
//                elementMappings.put(key, value);
//            }
//        }
//        return elementMappings;
//    }
//
//
//    /**
//     * 更新ID字段及Keys
//     *
//     * @param cmObject
//     * @param vmObject
//     */
//    private static void updateIDElement(IGspCommonObject cmObject, GspViewObject vmObject) {
//        // idEle
//        String idElementId = cmObject.getIDElement().getLabelID();
//        IGspCommonElement vmIdEle = vmObject.getElementByLabelId(idElementId);
//        vmObject.getColumnGenerateID().setElementID(vmIdEle.getID());
//    }
//
//    /**
//     * 更新Keys
//     *
//     * @param cmObject
//     * @param vmObject
//     */
//    public static void updateViewObjectKeys(IGspCommonObject cmObject, GspViewObject vmObject, String parentObjIDElementId) {
//        ArrayList<IGspCommonElement> vmElements = vmObject.getAllElementList(true);
//        // keys
//        GspAssociationKeyCollection keys = cmObject.getKeys();
//        if (keys.size() > 0) {
//            if (vmObject.getKeys().size() == 0) {
//                throw new RuntimeException(String.format("对象%1$s的外键关联未映射到视图对象对象。", cmObject.getName()));
//            }
//            for (GspAssociationKey key : vmObject.getKeys()) {
//                String beSourceEleId = getAssociationKeyBySourceElementId(keys, key.getSourceElement()).getSourceElement();
//                key.setSourceElement(getVmElementByMappedBizEleId(vmElements, beSourceEleId).getID());
//                key.setTargetElement(parentObjIDElementId);
//            }
//        }
//    }
//
//    private static GspCommonElement getVmElementByMappedBizEleId(ArrayList<IGspCommonElement> vmElements, String bizEleId) {
//        if (vmElements.isEmpty()) {
//            throw new RuntimeException("无映射字段id为'"+bizEleId+"'的字段。");
//        }
//        for (IGspCommonElement item : vmElements) {
//            if (bizEleId.equals(((GspViewModelElement) item).getMapping().getTargetObjId())) {
//                return (GspCommonElement)item;
//            }
//        }
//        throw new RuntimeException("无映射字段id为'"+bizEleId+"'的字段。");
//    }
//
//    private static GspAssociationKey getAssociationKeyBySourceElementId(GspAssociationKeyCollection keys, String sourceEleId) {
//        if (keys.size() == 0) {
//            throw new RuntimeException("无外键关联。");
//        }
//        for (GspAssociationKey item : keys) {
//            if (sourceEleId.equals(item.getSourceElement())) {
//                return item;
//            }
//        }
//        throw new RuntimeException("无外键关联。");
//    }
//
//
//    /**
//     * 更新分级信息
//     *
//     * @param cmObject
//     * @param vmObject
//     * @param viewElements
//     * @param elementMappings
//     */
//
////ORIGINAL LINE: internal static void UpdateHirarchyInfo(IGspCommonObject cmObject, GspViewObject vmObject, Dictionary<string, string> elementMappings, Dictionary<string, IGspCommonElement> viewElements, GspVoElementSourceType eleSourceType = GspVoElementSourceType.BeElement)
//    public static void updateHirarchyInfo(IGspCommonObject cmObject, GspViewObject vmObject, java.util.HashMap<String, String> elementMappings, java.util.HashMap<String, IGspCommonElement> viewElements, GspVoElementSourceType eleSourceType) {
////        if (cmObject.HirarchyInfo == null) {
////            vmObject.HirarchyInfo = null;
////            return;
////        }
////
////        String targetMetadataId = vmObject.getMapping().getTargetMetadataId();
////
////        var refHirarchyInfo = cmObject.HirarchyInfo.clone(vmObject);
////
////        var hirarchyInfo = cmObject.HirarchyInfo.clone(vmObject);
////
////        if (refHirarchyInfo.IsDetailElement != null) {
////
////            var isDetailElement = refHirarchyInfo.IsDetailElement;
////            if (elementMappings.containsKey(isDetailElement.getID())) {
////                String voEleId = elementMappings.get(isDetailElement.getID());
////                hirarchyInfo.IsDetailElement = viewElements.get(voEleId);
////            } else {
////                GspViewModelElement transElement = ToElement(refHirarchyInfo.IsDetailElement, null, targetMetadataId, eleSourceType);
////                vmObject.getContainElements().add(transElement);
////                hirarchyInfo.IsDetailElement = transElement;
////            }
////        }
////
////        if (refHirarchyInfo.LayerElement != null) {
////
////            var layerElement = refHirarchyInfo.LayerElement;
////            if (elementMappings.containsKey(layerElement.getID())) {
////                String voEleId = elementMappings.get(layerElement.getID());
////                hirarchyInfo.LayerElement = viewElements.get(voEleId);
////            } else {
////                GspViewModelElement transElement = ToElement(refHirarchyInfo.LayerElement, null, targetMetadataId, eleSourceType);
////                vmObject.getContainElements().add(transElement);
////                hirarchyInfo.LayerElement = transElement;
////            }
////        }
////
////        if (refHirarchyInfo.PathElement != null) {
////
////            var pathElement = refHirarchyInfo.PathElement;
////            if (elementMappings.containsKey(pathElement.getID())) {
////                String voEleId = elementMappings.get(pathElement.getID());
////                hirarchyInfo.PathElement = viewElements.get(voEleId);
////            } else {
////                GspViewModelElement transElement = ToElement(refHirarchyInfo.PathElement, null, targetMetadataId, eleSourceType);
////                vmObject.getContainElements().add(transElement);
////                hirarchyInfo.PathElement = transElement;
////            }
////        }
////
////        if (refHirarchyInfo.ParentElement != null) {
////
////            var parentElement = refHirarchyInfo.ParentElement;
////            if (elementMappings.containsKey(parentElement.getID())) {
////                String voEleId = elementMappings.get(parentElement.getID());
////                hirarchyInfo.ParentElement = viewElements.get(voEleId);
////            } else {
////                GspViewModelElement transElement = ToElement(refHirarchyInfo.ParentElement, null, targetMetadataId, eleSourceType);
////                vmObject.getContainElements().add(transElement);
////                hirarchyInfo.ParentElement = transElement;
////            }
////        }
////
////        if (refHirarchyInfo.ParentRefElement != null) {
////
////            var parentRefElement = refHirarchyInfo.ParentRefElement;
////            if (elementMappings.containsKey(parentRefElement.getID())) {
////                String voEleId = elementMappings.get(parentRefElement.getID());
////                hirarchyInfo.ParentRefElement = viewElements.get(voEleId);
////            } else {
////                GspViewModelElement transElement = ToElement(refHirarchyInfo.ParentRefElement, null, targetMetadataId, eleSourceType);
////                vmObject.getContainElements().add(transElement);
////                hirarchyInfo.ParentRefElement = transElement;
////            }
////        }
////
////        vmObject.HirarchyInfo = hirarchyInfo;
//    }
//
//    private static void convertChildObjects(IGspCommonObject cmObject, GspViewObject vmObject, String pkgName, String metadataId) {
//
//        for (IGspCommonObject childObject : cmObject.getContainChildObjects()) {
//            GspViewObject cmChildObject = toObject(childObject, pkgName, metadataId, vmObject.getIDElement().getID(),GspVoObjectSourceType.BeObject);
//            cmChildObject.setParentObject(vmObject);
//            vmObject.getContainChildObjects().add(cmChildObject);
//        }
//    }
////
//    private static void convertContainElements(IGspCommonObject cmObject, GspViewObject vmObject, String pkgName, String metadataId, GspVoObjectSourceType objSourceType) {
//        if (cmObject.getContainElements() == null || cmObject.getContainElements().size() < 1) {
//            return;
//        }
//        GspVoElementSourceType eleSourceType = getEleSourceType(objSourceType);
//
//        for (IGspCommonField cmElement : cmObject.getContainElements()) {
//            GspViewModelElement element = toElement((GspCommonElement) cmElement, pkgName, metadataId, eleSourceType);
//            element.setBelongObject(vmObject);
//            vmObject.getContainElements().add(element);
//        }
//    }
//
//    private static GspVoElementSourceType getEleSourceType(GspVoObjectSourceType objSourceType) {
//        switch (objSourceType) {
//            case BeObject:
//                return GspVoElementSourceType.BeElement;
//            case QoObject:
//                return GspVoElementSourceType.QoElement;
//            case VoObject:
//                return GspVoElementSourceType.VoElement;
//            default:
//                throw new RuntimeException("无效枚举值" + objSourceType);
//        }
//    }
//
//
//
//    //Element
//    public static GspViewModelElement toElement(IGspCommonElement element, String pkgName, String metadataId) {
//        return toElement(element, pkgName, metadataId);
//    }
//
//    public static GspViewModelElement toElement(IGspCommonElement element, String pkgName, String metadataId, GspVoElementSourceType sourceType) {
//        GspViewModelElement vmElement = new GspViewModelElement();
//
//        //① 字段基础信息
//        convertElementBasicInfo(element, vmElement, pkgName, metadataId, sourceType);
//        //② 关联
//        if (vmElement.getObjectType() == GspElementObjectType.Association) {
//            convertAssociation(element, vmElement, pkgName, metadataId, sourceType);
//        }
//        //③ 枚举
//        if (vmElement.getObjectType() == GspElementObjectType.Enum) {
//            convertEnumInfo(element, vmElement);
//        }
//        return vmElement;
//    }
//
//    private static void convertElementBasicInfo(IGspCommonElement cmEle, GspViewModelElement vmEle, String pkgName, String metadataId, GspVoElementSourceType sourceType) {
//        vmEle.setID(Guid.newGuid().toString());
//        vmEle.setCode(cmEle.getCode());
//        vmEle.setName(cmEle.getName());
//        vmEle.setLabelID(cmEle.getLabelID());
//        vmEle.setMDataType(cmEle.getMDataType());
//        vmEle.setLength(cmEle.getLength());
//        vmEle.setPrecision(cmEle.getPrecision());
//        vmEle.setObjectType(cmEle.getObjectType());
//        vmEle.setBelongModelID(cmEle.getBelongModelID());
//        vmEle.setIsMultiLanguage(cmEle.getIsMultiLanguage());
//        vmEle.setRefElementId(cmEle.getRefElementId());
//        vmEle.setIsCustomItem(cmEle.getIsCustomItem());
//        vmEle.setIsRequire(cmEle.getIsRequire());
//        vmEle.setIsRefElement(cmEle.getIsRefElement());
//        vmEle.setReadonly(cmEle.getReadonly());
//        vmEle.setIsFromAssoUdt(cmEle.getIsFromAssoUdt());
//
//        vmEle.setIsUdt(cmEle.getIsUdt());
//        //业务字段相关属性
//        if (vmEle.getIsUdt()) {
//            convertUdtInfo(cmEle, vmEle);
//        }
//
//        GspVoElementMapping tempVar = new GspVoElementMapping();
//        tempVar.setMapType(MappingType.Element);
//        tempVar.setTargetMetadataId(metadataId);
//        tempVar.setTargetMetadataPkgName(pkgName);
//        tempVar.setTargetObjId(cmEle.getID());
//        tempVar.setTargetElementId(cmEle.getID());
//        tempVar.setTargetObjectId(cmEle.getBelongObject().getID());
//        tempVar.setSourceType(sourceType);
//        vmEle.setMapping(tempVar);
//        // 字段源不同，进行不同处理的属性
//        convertEleSourceTypeRelatedProp(cmEle, vmEle, sourceType);
//        // 动态属性
//        vmEle.setDynamicPropSetInfo(cmEle.getDynamicPropSetInfo());
//    }
//
//    private static void convertEleSourceTypeRelatedProp(IGspCommonElement cmEle, GspViewModelElement vmEle, GspVoElementSourceType sourceType) {
//        switch (sourceType) {
//            case BeElement:
//            case QoElement:
//                //vmEle.setColumnID(cmEle.getColumnID());
//                //vmEle.setDefaultValue(cmEle.getDefaultValue()); // 20180921_测试用例评审_默认值不带出
//                //convertBillCodeConfig(vmEle.BillCodeConfig, cmEle.BillCodeConfig);
//                //vmEle.setDefaultValueType(cmEle.getDefaultValueType());
//                vmEle.setIsVirtualViewElement(false);
//                vmEle.setIsVirtual(false); //be带出，IsVirtual=false
//                vmEle.setIsRef(false);
//                break;
//            case VoElement:
//                vmEle.setColumnID(cmEle.getColumnID());
//                vmEle.setDefaultValue(cmEle.getDefaultValue());
//                convertBillCodeConfig(vmEle.getBillCodeConfig(), cmEle.getBillCodeConfig());
//                vmEle.setDefaultValueType(cmEle.getDefaultValueType());
//
//                vmEle.setIsVirtual(cmEle.getIsVirtual());
//                vmEle.setIsRef(cmEle.getIsRef());
//
//
//                GspViewModelElement sourceVoEle = (GspViewModelElement) cmEle;
//                vmEle.setIsVirtualViewElement(sourceVoEle.getIsVirtualViewElement());
//                vmEle.setIsBeckendOnly(sourceVoEle.getIsBeckendOnly());
//                vmEle.setImmediateSubmission(sourceVoEle.getImmediateSubmission());
//                vmEle.setShowInFilter(sourceVoEle.getShowInFilter());
//                vmEle.setShowInSort(sourceVoEle.getShowInSort());
//                vmEle.setVMHelpConfig(sourceVoEle.getVMHelpConfig());
//                vmEle.setHelpActions(sourceVoEle.getHelpActions());
//                vmEle.setExtendProperties(sourceVoEle.getExtendProperties());
//                vmEle.setExtendProperties(sourceVoEle.getExtendProperties());
//                break;
//            default:
//                throw new RuntimeException(errorToken + String.format("不存在的voEleMapping'%1$s'", sourceType.toString()) + errorToken);
//        }
//    }
//
//
//    private static void convertBillCodeConfig(ElementCodeRuleConfig config, ElementCodeRuleConfig sourceConfig) {
//        config.setCanBillCode(sourceConfig.getCanBillCode());
//        config.setBillCodeID(sourceConfig.getBillCodeID());
//        config.setBillCodeName(sourceConfig.getBillCodeID());
//        config.setCodeGenerateType(sourceConfig.getCodeGenerateType());
//        config.setCodeGenerateOccasion(sourceConfig.getCodeGenerateOccasion());
//    }
//
//    private static void convertEnumInfo(IGspCommonElement cmEle, GspViewModelElement vmEle) {
//        // 关联带出字段，无关联信息
//        if (cmEle.getIsRefElement()) {
//            return;
//        }
//        if (cmEle.getContainEnumValues() == null || cmEle.getContainEnumValues().size() < 1) {
//            throw new RuntimeException("找不到枚举值");
//        }
//        vmEle.setContainEnumValues(new GspEnumValueCollection());
//        for (GspEnumValue enumValue : cmEle.getContainEnumValues()) {
//            vmEle.getContainEnumValues().add(enumValue.clone());
//        }
//    }
//
//
//    private static void convertUdtInfo(IGspCommonElement cmEle, GspViewModelElement vmEle) {
//        vmEle.setUdtID(cmEle.getUdtID());
//        vmEle.setUdtName(cmEle.getUdtName());
//        vmEle.setUdtPkgName(cmEle.getUdtPkgName());
//        //TODO 后续UDt中集成VM属性后，需要获取UDT后赋值
//    }
//
//    //关联
//    private static void convertAssociation(IGspCommonElement cmEle, GspViewModelElement vmEle, String pkgName, String metadataId, GspVoElementSourceType sourceType) {
//        // 关联带出字段，无关联信息
//        if (cmEle.getIsRefElement()) {
//            return;
//        }
//        if (cmEle.getChildAssociations() == null || cmEle.getChildAssociations().size() < 1) {
//            throw new RuntimeException("找不到关联信息");
//        }
//        vmEle.setChildAssociations(new GspAssociationCollection());
//        for (GspAssociation cmAssociation : cmEle.getChildAssociations()) {
//            GspAssociation vmAssociation = getAssociation((GspAssociation) cmAssociation, pkgName, metadataId, vmEle.getID(), sourceType);
//            vmAssociation.setBelongElement(vmEle);
//            vmEle.getChildAssociations().add(vmAssociation);
//        }
//    }
//
//    private static GspAssociation getAssociation(GspAssociation cmAssociation, String pkgName, String metadataId, String vmElementId, GspVoElementSourceType souceType) {
//        Object tempVar = cmAssociation.clone();
//        GspAssociation vmAssociation = (GspAssociation) ((tempVar instanceof GspAssociation) ? tempVar : null);
//
//        vmAssociation.getKeyCollection().clear();
//        for (GspAssociationKey associationKey : cmAssociation.getKeyCollection()) {
//            associationKey.setTargetElement(vmElementId);
//            vmAssociation.getKeyCollection().add(associationKey);
//        }
//        vmAssociation.getRefElementCollection().clear();
//
//        for (IGspCommonField refElement : cmAssociation.getRefElementCollection()) {
//            GspViewModelElement vmRefElement = new GspViewModelElement();
//            convertElementBasicInfo((IGspCommonElement) refElement, vmRefElement, pkgName, metadataId, souceType);
//            if (refElement.getObjectType() == GspElementObjectType.Enum) {
//                convertEnumInfo((IGspCommonElement) refElement, vmRefElement);
//            }
//            vmRefElement.setParentAssociation(vmAssociation);
//            vmAssociation.getRefElementCollection().add(vmRefElement);
//        }
//
//
//        return vmAssociation;
//    }
//
//
//
//    //Action
//    private static void buildVmActions(BizMgrActionCollection beMgrActions, GspViewModel vm, String pkgName, String metadataId) {
//        if (beMgrActions == null) {
//            return;
//        }
//
//        for (var bizMgrAction : beMgrActions) {
//            if (bizMgrAction.getOpType() == BEOperationType.BizMgrAction) {
//                MappedBizAction vmAction = toMappedAction((BizMgrAction)bizMgrAction, metadataId, pkgName);
//                vm.getActions().add(vmAction);
//            }
//        }
//    }
//
//    public static MappedBizAction toMappedAction(BizMgrAction bizMgrAction, String metadataId, String pkgName) {
//        MappedBizAction tempVar = new MappedBizAction();
//        tempVar.setID(Guid.newGuid().toString());
//        tempVar.setCode(bizMgrAction.getCode());
//        tempVar.setName(bizMgrAction.getName());
//        tempVar.setReturnValue(getReturnValue(bizMgrAction));
//        tempVar.setComponentName(bizMgrAction.getComponentName());
//        ViewModelMapping mapping = new ViewModelMapping();
//        mapping.setMapType(MappingType.BizOperation);
//        mapping.setTargetMetadataId(metadataId);
//        mapping.setTargetMetadataPkgName(pkgName);
//        mapping.setTargetObjId(bizMgrAction.getID());
//        MappedBizAction vmAction = tempVar;
//        initParameterCollection(vmAction, bizMgrAction);
//        return vmAction;
//    }
//
//    private static void initParameterCollection(MappedBizAction vmAction, BizMgrAction action) {
//        if (action.getParameters() == null || action.getParameters().getCount() < 1) {
//            return;
//        }
//        for (int i = 0; i < action.getParameters().getCount(); i++) {
//            MappedBizActionParameter param = new MappedBizActionParameter();
//            transViewModelParameter(param, action.getParameters().getItem(i));
//            vmAction.getParameterCollection().add(param);
//        }
//    }
//
//
//    private static ViewModelReturnValue getReturnValue(BizMgrAction action) {
//        if (action.getReturnValue() == null || action.getReturnValue() instanceof BizVoidReturnType) {
//            return new ViewModelVoidReturnValue();
//        }
//        ViewModelReturnValue returnValue = new ViewModelReturnValue();
//        transViewModelParameter(returnValue, action.getReturnValue());
//        return returnValue;
//    }
//
//    private static void transViewModelParameter(ViewModelParameter vmParam, IBizParameter param) {
//        vmParam.setID(Guid.newGuid().toString());
//        vmParam.setAssembly(param.getAssembly());
//        vmParam.setClassName(param.getClassName());
//        vmParam.setDotnetClassName(((BizParameter)param).getNetClassName());
//        switch (param.getMode()) {
//            case IN:
//                vmParam.setMode(VMParameterMode.IN);
//                break;
//            case INOUT:
//                vmParam.setMode(VMParameterMode.INOUT);
//                break;
//            case OUT:
//                vmParam.setMode(VMParameterMode.OUT);
//                break;
//        }
//        switch (param.getCollectionParameterType()) {
//            case None:
//                vmParam.setCollectionParameterType(VMCollectionParameterType.None);
//                break;
//            case List:
//                vmParam.setCollectionParameterType(VMCollectionParameterType.List);
//                break;
//            case Array:
//                vmParam.setCollectionParameterType(VMCollectionParameterType.Array);
//                break;
//        }
//        vmParam.setParamCode(param.getParamCode());
//        vmParam.setParamDescription(param.getParamDescription());
//        vmParam.setParamName(param.getParamName());
//        switch (param.getParameterType()) {
//            case Boolean:
//                vmParam.setParameterType(VMParameterType.Boolean);
//                break;
//            case Custom:
//                vmParam.setParameterType(VMParameterType.Custom);
//                break;
//            case DateTime:
//                vmParam.setParameterType(VMParameterType.DateTime);
//                break;
//            case Decimal:
//                vmParam.setParameterType(VMParameterType.Decimal);
//                break;
//            case Double:
//                vmParam.setParameterType(VMParameterType.Double);
//                break;
//            case Int32:
//                vmParam.setParameterType(VMParameterType.Int32);
//                break;
//            case Object:
//                vmParam.setParameterType(VMParameterType.Object);
//                break;
//            case String:
//                vmParam.setParameterType(VMParameterType.String);
//                break;
//            default:
//                throw new IllegalStateException("Unexpected value: " + param.getParameterType());
//        }
//
//
//    }
////TODO TASK: There is no preprocessor in Java:
//
////
////TODO TASK: There is no preprocessor in Java:
////    //Vo
////TODO TASK: There is no preprocessor in Java:
////
////
////TODO TASK: There is no preprocessor in Java:
////    //Qo
////
////    //public static GspViewModel convertQoToViewModel(QueryObject qo, string pkgName, string metadataId, string idElementId, List<string> eleIdList=null)
////    //{
////    //	QueryNode node = qo.QueryNode;
////    //	GspViewModel vm = new GspViewModel();
////
////    //	// 基本信息
////    //	convertQoToVmBasicInfo(qo, vm, pkgName, metadataId);
////
////    //	// Elements
////    //	var mainObj = ToObject(node, pkgName, metadataId, null, GspVoObjectSourceType.QoObject);
////
////    //	if (eleIdList != null && eleIdList.Count > 0)
////    //	{
////    //		var elements = mainObj.ContainElements.Clone() as GspElementCollection;
////    //		mainObj.ContainElements.clear();
////    //		foreach (var eleId in eleIdList)
////    //		{
////    //			var ve = elements.Find(ele => ele.getID() == eleId) as GspViewModelElement;
////    //			if (ve == null)
////    //			{
////    //				throw new Exception(errorToken + $"结果集中没有id='{eleId}'的字段。" + errorToken);
////    //			}
////    //			// 兼容queryNode中字段BelongObject为空问题
////    //			ve.Mapping.TargetObjectId = node.getID();
////    //			mainObj.ContainElements.add(ve);
////    //		}
////    //	}
////
////    //	// IDElement
////    //	var columnGenerateId = new GspColumnGenerate();
////    //	columnGenerateId.ElementID = idElementId;
////    //	vm.MainObject.ColumnGenerateID = columnGenerateId;
////
////    //	vm.MainObject = mainObj;
////
////    // // BelongModel递归赋值
////    // vm.MainObject.BelongModel = vm;
////    // vm.MainObject.BelongModelID = vm.getID();
////    // SetChildObjectBelongModel(vm.MainObject, vm);
////    //	return vm;
////    //}
////
////
////    //   private static void convertQoToVmBasicInfo(QueryObject qo, GspViewModel vm, string pkgName, string metadataId)
////    //   {
////    //	vm.getID() = Guid.newGuid().ToString();
////    //    vm.Code = string.Format("{0}{1}", qo.Code, AUTO_GEN_VM_SUFFIX);
////    //    vm.Name = string.Format("{0}_{1}", AUTO_GEN_VM_PREFIX, qo.Name);
////    //    vm.IsVirtual = false; //qo带出，IsVirtual=false
////    //    vm.Mapping = new ViewModelMapping
////    //    {
////    //	    //MapType = MappingType.BizEntity,
////    //	    //TargetMetadataType = MetadataType.BizEntity,
////    //	    TargetMetadataPkgName = pkgName,
////    //	    TargetMetadataId = metadataId,
////    //	    TargetObjId = qo.getID()
////    //    };
////    //}
////
////TODO TASK: There is no preprocessor in Java:
////
////
////TODO TASK: There is no preprocessor in Java:
////    //Tool
////
////    /**
////     * 根据输入字段ID列表将co字段转换为vo字段集合
////     *
////     * @param co
////     * @param eleIdList
////     * @param pkgName
////     * @param metaId
////     * @param sourceType
////     * @return
////     */
////
//////ORIGINAL LINE: public static GspElementCollection convertPartialElementsToVoElements(IGspCommonObject co, List<string> eleIdList, string pkgName, string metaId, GspVoElementSourceType sourceType = GspVoElementSourceType.BeElement)
////    public static GspElementCollection convertPartialElementsToVoElements(IGspCommonObject co, java.util.ArrayList<String> eleIdList, String pkgName, String metaId, GspVoElementSourceType sourceType) {
////        GspElementCollection voElements = new GspElementCollection(null);
////        if (eleIdList != null && eleIdList.size() > 0) {
////
////            for (var eleId : eleIdList) {
////
////                var bizElement = co.FindElement(eleId);
////                GspViewModelElement voElement = ToElement(bizElement, pkgName, metaId, sourceType);
////                voElement.getMapping().setTargetObjectId(co.getID());
////                voElements.add(voElement);
////            }
////        }
////        return voElements;
////    }
////TODO TASK: There is no preprocessor in Java:
////
////
//}