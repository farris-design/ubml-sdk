/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.vo.definition.collection;

import java.io.Serializable;
import org.openatom.ubml.model.common.definition.cef.IGspCommonField;
import org.openatom.ubml.model.common.definition.commonmodel.collection.GspElementCollection;
import org.openatom.ubml.model.vo.definition.GspViewModelElement;
import org.openatom.ubml.model.vo.definition.GspViewObject;

/**
 * The Collection Of View Model Element
 *
 * @ClassName: VMElementCollection
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class VMElementCollection extends GspElementCollection implements Serializable {

    /**
     * 字段集合构造函数
     *
     * @param owner 所属视图对象结点
     */
    public VMElementCollection(GspViewObject owner) {
        super(owner);
    }

    /**
     * 根据ID获取节点元素
     *
     * @param id 节点元素ID
     * @return 节点元素
     */
    @Override
    public final GspViewModelElement getItem(String id) {
        for (IGspCommonField item : this) {
            if (item instanceof GspViewModelElement) {
                return (GspViewModelElement) item;
            }
        }
        return null;
    }

    /**
     * 重载Equals方法
     *
     * @param obj 要比较的对象
     * @return 如果当前对象等于 other 参数，则为 true；否则为 false。
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        return equals((VMElementCollection) obj);
    }

    /**
     * 当前对象是否等于同一类型的另一个对象。
     *
     * @param other 与此对象进行比较的对象。
     * @return 如果当前对象等于 other 参数，则为 true；否则为 false。
     */
    protected boolean equals(VMElementCollection other) {
        if (this.size() != other.size()) {
            return false;
        }
        for (IGspCommonField item : this) {
            IGspCommonField otherItem = other.getItem(item.getID());
            if (otherItem == null) {
                return false;
            }
            if (!item.equals(otherItem)) {
                return false;
            }
        }
        return true;
    }

    /**
     * 克隆
     *
     * @return VO节点元素集合
     */
    @Override
    public final VMElementCollection clone() {
        VMElementCollection collections;
        collections = new VMElementCollection((GspViewObject) getParentObject());
        for (IGspCommonField item : this) {
            GspViewModelElement tempVar = ((GspViewModelElement) item).clone();
            collections.add(tempVar);
        }
        return collections;
    }
}
