/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.be.definition.json.operation;

import org.openatom.ubml.model.be.definition.operation.collection.ValidationCollection;

/**
 * The Josn Deserializer Of Validation Collection
 *
 * @ClassName: ValCollectionDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ValCollectionDeserializer extends BizOperationCollectionDeserializer<ValidationCollection> {
    @Override
    protected ValidationCollection createCollection() {
        return new ValidationCollection();
    }

    @Override
    protected BizValidationDeserializer createDeserializer() {
        return new BizValidationDeserializer();
    }
}
