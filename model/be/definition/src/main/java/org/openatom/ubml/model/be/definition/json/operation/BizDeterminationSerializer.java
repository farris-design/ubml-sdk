/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.be.definition.json.operation;

import com.fasterxml.jackson.core.JsonGenerator;
import java.util.HashMap;
import java.util.Map;
import org.openatom.ubml.model.be.definition.beenum.BETriggerTimePointType;
import org.openatom.ubml.model.be.definition.beenum.RequestNodeTriggerType;
import org.openatom.ubml.model.be.definition.collection.DtmElementCollection;
import org.openatom.ubml.model.be.definition.common.BizEntityJsonConst;
import org.openatom.ubml.model.be.definition.operation.BizOperation;
import org.openatom.ubml.model.be.definition.operation.Determination;
import org.openatom.ubml.model.common.definition.cef.json.SerializerUtils;

/**
 * The  Josn Serializer Of Entity Determination
 *
 * @ClassName: BizDeterminationSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class BizDeterminationSerializer extends BizOperationSerializer<Determination> {
    @Override
    protected void writeExtendOperationBaseProperty(JsonGenerator writer, BizOperation op) {

    }

    @Override
    protected void writeExtendOperationSelfProperty(JsonGenerator writer, BizOperation op) {
        Determination dtm = (Determination)op;
        SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.DETERMINATION_TYPE, dtm.getDeterminationType().getValue());
        int triggerTimePointType = 0;
        for (BETriggerTimePointType timePointType : dtm.getTriggerTimePointType()) {
            triggerTimePointType += timePointType.getValue();
        }
        SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.TRIGGER_TIME_POINT_TYPE, triggerTimePointType);

        int requestNodeTriggerType = 0;
        for (RequestNodeTriggerType requestNodeTrigger : dtm.getRequestNodeTriggerType()) {
            requestNodeTriggerType += requestNodeTrigger.getValue();
        }
        SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.REQUEST_NODE_TRIGGER_TYPE, requestNodeTriggerType);

        writeRequestElements(writer, dtm);
        writeRequestChildElements(writer, dtm);
    }

    private void writeRequestElements(JsonGenerator writer, Determination dtm) {
        SerializerUtils.writePropertyName(writer, BizEntityJsonConst.REQUEST_ELEMENTS);
        writeDtmElementCollection(writer, dtm.getRequestElements());
    }

    private void writeDtmElementCollection(JsonGenerator writer, DtmElementCollection childElementsIds) {
        //[
        SerializerUtils.WriteStartArray(writer);
        if (childElementsIds.size() > 0) {
            for (String item : childElementsIds) {
                SerializerUtils.writePropertyValue_String(writer, item);
            }
        }
        //]
        SerializerUtils.WriteEndArray(writer);
    }

    private void writeRequestChildElements(JsonGenerator writer, Determination dtm) {
        SerializerUtils.writePropertyName(writer, BizEntityJsonConst.REQUEST_CHILD_ELEMENTS);
        HashMap<String, DtmElementCollection> dic = dtm.getRequestChildElements();
        SerializerUtils.WriteStartArray(writer);
        if (dic != null && dic.size() > 0) {
            for (Map.Entry<String, DtmElementCollection> item : dic.entrySet()) {
                SerializerUtils.writeStartObject(writer);
                SerializerUtils.writePropertyValue(writer, BizEntityJsonConst.REQUEST_CHILD_ELEMENT_KEY, item.getKey());
                SerializerUtils.writePropertyName(writer, BizEntityJsonConst.REQUEST_CHILD_ELEMENT_VALUE);
                writeDtmElementCollection(writer, item.getValue());
                SerializerUtils.writeEndObject(writer);
            }
        }
        SerializerUtils.WriteEndArray(writer);
    }
}
