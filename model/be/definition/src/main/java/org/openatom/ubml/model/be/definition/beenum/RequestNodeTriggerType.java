/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.be.definition.beenum;

/**
 * The Type Of Business Entity Request Node Trigger Type
 *
 * @ClassName: RequestNodeTriggerType
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public enum RequestNodeTriggerType {
    /**
     * 不执行
     */
    None(0),

    /**
     * 实例创建时执行
     */
    Created(1),

    /**
     * 实例更新时执行
     */
    Updated(2),

    /**
     * 实例删除时执行
     */
    Deleted(4),

    /**
     * 校验或者自动计算时(分别用于Validation、Determination)
     */
    ValidateOrDetermine(8),

    /**
     * 数据加载时，Determination专用。
     */
    Load(16);

    private static java.util.HashMap<Integer, RequestNodeTriggerType> mappings;
    private int intValue;

    private RequestNodeTriggerType(int value) {
        intValue = value;
        RequestNodeTriggerType.getMappings().put(value, this);
    }

    private synchronized static java.util.HashMap<Integer, RequestNodeTriggerType> getMappings() {
        if (mappings == null) {
            mappings = new java.util.HashMap<Integer, RequestNodeTriggerType>();
        }
        return mappings;
    }

    public static RequestNodeTriggerType forValue(int value) {
        return getMappings().get(value);
    }

    public int getValue() {
        return intValue;
    }
}