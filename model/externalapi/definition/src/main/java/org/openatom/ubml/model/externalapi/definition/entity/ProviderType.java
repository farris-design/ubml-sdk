/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.externalapi.definition.entity;

/**
 * EApi的provider类型
 *
 * @ClassName: ProviderType
 * @Author: Fynn Qi
 * @Date: 2020/12/23 16:00
 * @Version: V1.0
 */
public enum ProviderType {

    /**
     * 视图对象-高级型
     */
    VO_ADVANCE,

    /**
     * 视图对象-经典型
     */
    VO_SIMPLE;

}
