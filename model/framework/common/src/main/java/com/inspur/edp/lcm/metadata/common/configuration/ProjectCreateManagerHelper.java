/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.common.configuration;

import com.inspur.edp.lcm.metadata.api.ConfigData.gspproject.ProjectConfiguration;
import com.inspur.edp.lcm.metadata.spi.IProjectCreateManager;
import java.util.List;
import java.util.Objects;
import org.springframework.util.StringUtils;

public class ProjectCreateManagerHelper extends GspProjectConfigurationLoader {
    private static ProjectCreateManagerHelper instance;

    public static ProjectCreateManagerHelper getInstance() {
        if (Objects.isNull(instance)) {
            instance = new ProjectCreateManagerHelper();
        }
        return instance;
    }

    private ProjectCreateManagerHelper() {

    }

    /// <summary>
    /// 返回各元数据序列化器
    /// </summary>
    /// <param name="typeName"></param>
    /// <returns></returns>
    public IProjectCreateManager getManager(String typeName) {
        IProjectCreateManager manager = null;
        ProjectConfiguration data = getGspProjectConfigurationData(typeName);
        if (!Objects.isNull(data) && !Objects.isNull(data.getCreateConfigData()) && !StringUtils.isEmpty(data.getCreateConfigData().getName())) {
            Class<?> cls = null;
            try {
                cls = Class.forName(data.getCreateConfigData().getName());
                manager = (IProjectCreateManager) cls.newInstance();
            } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        }
        return manager;
    }

    public List<ProjectConfiguration> getProjectTypeList() {
        return loadGspProjectConfigurations();
    }
}
