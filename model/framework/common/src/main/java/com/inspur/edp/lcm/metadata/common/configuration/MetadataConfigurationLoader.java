/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.common.configuration;

import com.inspur.edp.lcm.metadata.api.ConfigData.MetadataConfiguration;
import com.inspur.edp.lcm.metadata.common.FileServiceImp;
import io.iec.edp.caf.common.environment.EnvironmentUtil;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;

/**
 * @Classname MetadataConfigurationLoader
 * @Description 配置文件实体
 * @Date 2019/7/23 14:44
 * @Created by zhongchq
 * @Version 1.0
 */
public class MetadataConfigurationLoader implements Serializable {

    public static List<MetadataConfiguration> metadataConfigurations;
    private static String fileName = "config/platform/common/lcm_metadataextend.json";
    private static final String SECTION_NAME = "MetadataConfiguration";

    public MetadataConfigurationLoader() {
        loadMetadataConfigurations();
    }

    public static List<MetadataConfiguration> getMetadataConfigurations() {
        return metadataConfigurations;
    }

    public static void setMetadataConfigurations(
        List<MetadataConfiguration> metadataConfigurations) {
        MetadataConfigurationLoader.metadataConfigurations = metadataConfigurations;
    }

    /**
     * @param
     * @return java.util.List<com.inspur.gsp.lcm.metadata.entity.MetadataConfiguration>
     * @throws
     * @author zhongchq
     * @description 类初始化时反序列化Json到实体
     * @date 15:40 2019/7/23
     **/
    protected List<MetadataConfiguration> loadMetadataConfigurations() {
        MetadataServiceHelper metadataServiceHelper = new MetadataServiceHelper();
        try {
            FileServiceImp fileService = new FileServiceImp();
            fileName = fileService.getCombinePath(EnvironmentUtil.getServerRTPath(), fileName);

            if (metadataConfigurations == null || metadataConfigurations.size() <= 0) {
                metadataConfigurations = metadataServiceHelper.getMetadataConfigurationList(fileName, SECTION_NAME);
                return metadataConfigurations;
            } else {
                return metadataConfigurations;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected MetadataConfiguration getMetadataConfigurationData(String typeName) {
        loadMetadataConfigurations();
        if (metadataConfigurations != null && metadataConfigurations.size() > 0) {
            for (MetadataConfiguration data : metadataConfigurations) {
                if (data.getCommon().getTypeCode().equalsIgnoreCase(typeName)) {
                    return data;
                }
            }
        } else {
            return null;
        }
        return null;
    }
}
