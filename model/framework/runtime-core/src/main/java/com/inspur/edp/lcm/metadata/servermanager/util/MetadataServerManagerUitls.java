/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.servermanager.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.inspur.edp.lcm.metadata.api.entity.Metadata4Ref;
import com.inspur.edp.lcm.metadata.api.entity.Metadata4RefDto;
import com.inspur.edp.lcm.metadata.api.entity.MetadataDto;
import com.inspur.edp.lcm.metadata.api.entity.MetadataRTFilter;
import com.inspur.edp.lcm.metadata.common.MetadataDtoConverter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MetadataServerManagerUitls {

    public List<Metadata4RefDto> buildMetadata4RefDto(List<Metadata4Ref> metadata4Refs) {
        if (metadata4Refs == null || metadata4Refs.size() <= 0) {
            return null;
        }
        List<Metadata4RefDto> metadata4RefDtos = new ArrayList<>();
        for (Metadata4Ref metadata4Ref : metadata4Refs) {
            Metadata4RefDto dto = new Metadata4RefDto();
            MetadataDto metadataDto = MetadataDtoConverter.asDto(metadata4Ref.getMetadata());
            dto.setPackageHeader(metadata4Ref.getPackageHeader());
            dto.setMetadata(metadataDto);
            dto.setServiceUnitInfo(metadata4Ref.getServiceUnitInfo());
            metadata4RefDtos.add(dto);
        }

        return metadata4RefDtos;
    }

    public String serializeMetadata4RefDtos(List<Metadata4RefDto> metadataDtos) {
        String result = "";
        ObjectMapper mapper = new ObjectMapper();
        mapper.setPropertyNamingStrategy(PropertyNamingStrategy.UPPER_CAMEL_CASE);
        try {
            result = mapper.writeValueAsString(metadataDtos);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return result;
    }

    public MetadataRTFilter deserializerMetadataRTFilter(String content) {
        MetadataRTFilter result = null;
        ObjectMapper mapper = new ObjectMapper();
        mapper.setPropertyNamingStrategy(PropertyNamingStrategy.UPPER_CAMEL_CASE);
        mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        try {
            result = mapper.readValue(content, MetadataRTFilter.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}
