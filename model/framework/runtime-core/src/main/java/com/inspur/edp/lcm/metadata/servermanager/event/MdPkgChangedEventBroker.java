/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.servermanager.event;

import io.iec.edp.caf.commons.event.CAFEventArgs;
import io.iec.edp.caf.commons.event.EventBroker;
import io.iec.edp.caf.commons.event.IEventListener;
import io.iec.edp.caf.commons.event.config.EventListenerData;
import io.iec.edp.caf.commons.event.config.EventListenerSettings;

/**
 * @author zhaoleitr
 */
public class MdPkgChangedEventBroker extends EventBroker {

    public MdPkgChangedEventManager mdPkgChangedEventManager;

    public MdPkgChangedEventBroker(EventListenerSettings settings) {
        super(settings);
        this.mdPkgChangedEventManager = new MdPkgChangedEventManager();
        this.init();
    }

    @Override
    protected void onInit() {
        this.eventManagerCollection.add(mdPkgChangedEventManager);
    }

    public void fireMdPkgAddedEvent(CAFEventArgs e) {
        this.mdPkgChangedEventManager.fireMdPkgAddedEvent(e);

    }

    public void fireMdPkgChangedEvent(CAFEventArgs e) {
        this.mdPkgChangedEventManager.fireMdPkgChangedEvent(e);
    }

    /**
     * 测试异常是否被抛出
     */
    public IEventListener createEventListenerTest(EventListenerData eventListenerData) {
        return this.createEventListener(eventListenerData);
    }

    /**
     * 测试注销事件
     *
     * @param eventListener
     */
    public void removeListenerTest(IEventListener eventListener) {
        this.removeListener(eventListener);
    }
}
