/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.jittojava.core;

import com.inspur.edp.jittojava.context.service.CommonService;
import com.inspur.edp.lcm.metadata.common.Utils;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Classname CommonServiceCore Description 实现类 Date 2019/8/8 16:42
 *
 * @author zhongchq
 * @version 1.0
 */
public class CommonServiceCore implements CommonService {

    @Override
    public String getProjectPath(String absJavaPath) {
        //java目录下进行判断
        File file = new File(absJavaPath);
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            if (files.length == 0) {
                return null;
            }
            //遍历java目录下目录
            for (int i = 0; i < files.length; i++) {
                //对目录进行判断
                if (files[i].isDirectory()) {
                    File[] fileInPoject = files[i].listFiles();
                    for (File file1 : fileInPoject) {
                        if (file1.isFile() && file1.toString().contains("pom.xml")) {
                            return Utils.handlePath(file1.getParent().substring(file1.toString().indexOf("java")));
                        }
                    }
                }
            }
        }
        return null;
    }

    @Override
    public List<String> getJarPath(String absJavaPath) {
        Path path = Paths.get(absJavaPath);
        List<String> filename = new ArrayList<>();
        try {
            Files.walkFileTree(path
                , new SimpleFileVisitor<Path>() {
                    // 访问文件时触发
                    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                        if (file.toString().endsWith(".jar")) {
                            String filepaths = file.toString();
                            if (filepaths.contains("out") || filepaths.contains("target")) {
                                filename.add(Utils.handlePath(filepaths.substring(filepaths.indexOf("java"))));
                            }
                            return FileVisitResult.CONTINUE;
                        }
                        return FileVisitResult.CONTINUE;
                    }
//
//                        // 访问目录时触发
//                        @Override
//                        public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
//                            if (dir.endsWith("target")) {
//                               String targetpath = dir.getParent().toAbsolutePath().toString().su
//                               targetpath.substring("java");
//                                list.add(targetpath);
//                            }
//                            return FileVisitResult.CONTINUE;
//                        }
                });
        } catch (IOException e) {
            e.printStackTrace();
        }
        Collections.sort(filename);
        return filename;
    }
}
