/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.jittojava.core;

import com.inspur.edp.jittojava.context.GenerateService;
import com.inspur.edp.jittojava.context.entity.MavenDependency;
import com.inspur.edp.lcm.metadata.api.service.FileService;
import com.inspur.edp.lcm.metadata.devcommon.ManagerUtils;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import org.codehaus.plexus.util.StringUtils;

public class GenerateServiceImp implements GenerateService {

    GenerateServiceCore generateServiceCore = new GenerateServiceCore();

    @Override
    public void generate(String path) throws Exception {
        String absolutePath = ManagerUtils.getAbsolutePath(path);
        String mavenPath = ManagerUtils.getMavenStoragePath();
        generateServiceCore.generate(absolutePath, mavenPath);
    }

    @Override
    public void generateApi(String path) throws Exception {
        String absolutePath = ManagerUtils.getAbsolutePath(path);
        String mavenPath = ManagerUtils.getMavenStoragePath();
        generateServiceCore.generateApi(absolutePath, mavenPath);
    }

    @Override
    public void modifyPom(String projPath, List<MavenDependency> mavenDependencyList) {
        String absolutePath = ManagerUtils.getAbsolutePath(projPath);
        generateServiceCore.modifyPom(absolutePath, mavenDependencyList);
    }

    @Override
    public void addProperties(String codePath, String propertyKey, String property) {
        String factoriesFilePath = Paths.get(codePath).getParent().resolve(JitUtils.getSpringPeopertiesPath()).toString();
        FileService fileService = SpringBeanUtils.getBean(FileService.class);
        if (!fileService.isFileExist(factoriesFilePath)) {
            try {
                fileService.createDirectory(Paths.get(factoriesFilePath).getParent().toString());
                fileService.createFile(factoriesFilePath);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        Properties properties = loadProperties(factoriesFilePath);
        String valueStr = properties.getProperty(propertyKey);
        if (StringUtils.isEmpty(valueStr)) {
            properties.put(propertyKey, property);
            updatePropertyFile(factoriesFilePath, properties);
        } else {
            List<String> values = Arrays.asList(valueStr.split(",").clone());
            if (!values.contains(property)) {
                valueStr = valueStr + "," + property;
                properties.put(propertyKey, valueStr);
                updatePropertyFile(factoriesFilePath, properties);
            }
        }
    }

    public Properties loadProperties(String paramFile) {
        Properties props = new Properties();//使用Properties类来加载属性文件
        try {
            FileInputStream iFile = new FileInputStream(paramFile);
            props.load(iFile);
            iFile.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return props;
    }

    private void updatePropertyFile(String filePath, Properties properties) {
        try {
            FileService fileService = SpringBeanUtils.getBean(FileService.class);
            fileService.fileUpdate(filePath, "", false);
            FileOutputStream oFile = new FileOutputStream(filePath, true);
            properties.store(oFile, "");
            oFile.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
