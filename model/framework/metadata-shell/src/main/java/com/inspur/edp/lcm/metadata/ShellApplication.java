/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata;

import org.jline.utils.AttributedString;
import org.jline.utils.AttributedStyle;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import static com.inspur.edp.lcm.metadata.shell.Utils.SPRING_SHELL_FLAG_NAME;
import static com.inspur.edp.lcm.metadata.shell.Utils.SPRING_SHELL_FLAG_TRUE;

@SpringBootApplication(scanBasePackages = {"io.iec.edp", "com.inspur"})
public class ShellApplication {
    public static void main(String[] args) {
        System.setProperty(SPRING_SHELL_FLAG_NAME, SPRING_SHELL_FLAG_TRUE);
        SpringApplication.run(ShellApplication.class, args);
    }

    @Bean
    public org.springframework.shell.jline.PromptProvider myPromptProvider() {
        return () -> new AttributedString("lcm-metadata-shell:>", AttributedStyle.DEFAULT.foreground(AttributedStyle.YELLOW));
    }
}
