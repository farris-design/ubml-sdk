/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.core.event;

import io.iec.edp.caf.commons.event.CAFEventArgs;
import io.iec.edp.caf.commons.event.EventBroker;
import io.iec.edp.caf.commons.event.config.EventListenerSettings;

public class DirEventBroker extends EventBroker {
    private DirEventManager dirEventManager;

    public DirEventBroker(EventListenerSettings settings) {
        super(settings);
        this.dirEventManager = new DirEventManager();
        this.init();
    }

    @Override
    protected void onInit() {
        this.eventManagerCollection.add(dirEventManager);
    }

    public void fireDirDeletingEvent(CAFEventArgs args) {
        this.dirEventManager.fireDirDeletingEvent(args);
    }

    public void fireDirDeletedEvent(CAFEventArgs args) {
        this.dirEventManager.fireDirDeletedEvent(args);
    }
}
