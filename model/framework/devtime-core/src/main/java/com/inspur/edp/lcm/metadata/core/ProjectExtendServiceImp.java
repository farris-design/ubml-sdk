/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.core;

import com.inspur.edp.lcm.metadata.api.entity.DbConnectionInfo;
import com.inspur.edp.lcm.metadata.api.service.ProjectExtendService;
import com.inspur.edp.lcm.metadata.devcommon.ManagerUtils;
import java.util.List;

public class ProjectExtendServiceImp implements ProjectExtendService {
    private final ProjectExtendCoreService projectExtendCoreService = new ProjectExtendCoreService();

    @Override
    public void deploy(DbConnectionInfo dBConnectionInfo, String serverPath, String projPath, String restart) {
        String absolutePath = ManagerUtils.getAbsolutePath(projPath);
        projectExtendCoreService.deploy(dBConnectionInfo, serverPath, absolutePath, restart);
    }

    @Override
    public void extract(String path) {
        String absolutePath = ManagerUtils.getAbsolutePath(path);
        projectExtendCoreService.extract(absolutePath);
    }

    @Override
    public void migration(String path) {
        String absolutePath = ManagerUtils.getAbsolutePath(path);
        projectExtendCoreService.migration(absolutePath);
    }

    @Override
    public String getDeployStatus(String sign) {
        return null;
    }

    @Override
    public List<String> batchCompile(String path, String exts, String disabledExts) {
        String absolutePath = ManagerUtils.getAbsolutePath(path);
        String packagePath = ManagerUtils.getMetadataPackageLocation();
        return projectExtendCoreService.batchCompile(absolutePath, exts, disabledExts, packagePath);
    }
}
