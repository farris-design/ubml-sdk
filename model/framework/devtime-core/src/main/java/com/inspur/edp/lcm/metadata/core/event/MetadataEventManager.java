/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.core.event;

import com.inspur.edp.lcm.metadata.spi.event.MetadataEventListener;
import io.iec.edp.caf.commons.event.CAFEventArgs;
import io.iec.edp.caf.commons.event.EventManager;
import io.iec.edp.caf.commons.event.IEventListener;

public class MetadataEventManager extends EventManager {

    enum MetadataEventType {
        metadataSavingEvent,
        metadataSavedEvent,
        metadataDeletingEvent,
        metadataDeletedEvent
    }

    @Override
    public String getEventManagerName() {
        return "MetadataEventManager";
    }

    @Override
    public boolean isHandlerListener(IEventListener iEventListener) {
        return iEventListener instanceof MetadataEventListener;
    }

    @Override
    public void addListener(IEventListener iEventListener) {
        checkHandlerListener(iEventListener);
        MetadataEventListener metadataEventListener = (MetadataEventListener) iEventListener;
        this.addEventHandler(MetadataEventType.metadataSavingEvent, metadataEventListener, "fireMetadataSavingEvent");
        this.addEventHandler(MetadataEventType.metadataSavedEvent, metadataEventListener, "fireMetadataSavedEvent");
        this.addEventHandler(MetadataEventType.metadataDeletingEvent, metadataEventListener, "fireMetadataDeletingEvent");
        this.addEventHandler(MetadataEventType.metadataDeletedEvent, metadataEventListener, "fireMetadataDeletedEvent");
    }

    @Override
    public void removeListener(IEventListener iEventListener) {
        checkHandlerListener(iEventListener);
        MetadataEventListener metadataEventListener = (MetadataEventListener) iEventListener;
        this.removeEventHandler(MetadataEventType.metadataSavingEvent, metadataEventListener, "fireMetadataSavingEvent");
        this.removeEventHandler(MetadataEventType.metadataSavedEvent, metadataEventListener, "fireMetadataSavedEvent");
        this.removeEventHandler(MetadataEventType.metadataDeletingEvent, metadataEventListener, "fireMetadataDeletingEvent");
        this.removeEventHandler(MetadataEventType.metadataDeletedEvent, metadataEventListener, "fireMetadataDeletedEvent");
    }

    public void fireMetadataSavingEvent(CAFEventArgs args) {
        this.fire(MetadataEventType.metadataSavingEvent, args);
    }

    public void fireMetadataSavedEvent(CAFEventArgs args) {
        this.fire(MetadataEventType.metadataSavedEvent, args);
    }

    public void fireMetadataDeletingEvent(CAFEventArgs args) {
        this.fire(MetadataEventType.metadataDeletingEvent, args);
    }

    public void fireMetadataDeletedEvent(CAFEventArgs args) {
        this.fire(MetadataEventType.metadataDeletedEvent, args);
    }

    private void checkHandlerListener(IEventListener iEventListener) {
        if (!isHandlerListener(iEventListener)) {
            throw new RuntimeException("指定的监听者" + iEventListener.getClass().getName()
                + "没有实现" + MetadataEventListener.class.getName() + "接口");
        }
    }
}
