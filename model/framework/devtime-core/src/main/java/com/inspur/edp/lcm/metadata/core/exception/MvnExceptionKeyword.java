/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.core.exception;

public class MvnExceptionKeyword {
    public static final String NOT_FIND_SYMBOL = "找不到符号";
    public static final String SYMBOL = "符号:";
    public static final String SYMBOL_LOCATION = "位置";

    public static final String NOT_EXIST = "不存在";

    public static final String NOT_ABSTRACT = "不是抽象的";

    public static final String DEFINED_NAME = "已定义";

    public static final String NOT_BE_RESOLVED = "The following artifacts could not be resolved";

    public static final String CLASS_NOT_AVAILABLE = "无法将类";
    public static final String NEED = "需要";
    public static final String FIND = "找到";
    public static final String REASON = "无法将类";

    public static final String NOT_ACCESSIBLE = "无法访问";
    public static final String NOT_FIND = "找不到";

    public static final String ERROR_IN_OPENING_ZIP_FILE = "error in opening zip file";
}
