/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.core.sync;

import com.inspur.edp.lcm.metadata.common.FileServiceImp;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class CopyToPackagesRunnable implements Runnable {
    private List<String> downloadMdpkgPaths;
    private String packagePath;
    protected final FileServiceImp fileServiceImp = new FileServiceImp();

    @Override
    public void run() {
        downloadMdpkgPaths.stream().parallel().forEach(path -> {
            String mdpkgName = new File(path).getName();
            String desPackageDir = fileServiceImp.getCombinePath(packagePath, mdpkgName);
            if (!fileServiceImp.isDirectoryExist(desPackageDir)) {
                fileServiceImp.createDirectory(desPackageDir);
            }
            String desPackagePath = fileServiceImp.getCombinePath(desPackageDir, mdpkgName);
            try {
                fileServiceImp.fileCopy(path, desPackagePath);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public void setPackagePath(String packagePath) {
        this.packagePath = packagePath;
    }

    public void setDownloadMdpkgPaths(List<String> downloadMdpkgPaths) {
        this.downloadMdpkgPaths = downloadMdpkgPaths;
    }
}
