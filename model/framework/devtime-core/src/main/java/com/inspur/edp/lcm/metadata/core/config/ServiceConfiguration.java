/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.core.config;

import com.inspur.edp.lcm.metadata.api.service.GspProjectService;
import com.inspur.edp.lcm.metadata.api.service.MetadataProjectService;
import com.inspur.edp.lcm.metadata.api.service.MetadataService;
import com.inspur.edp.lcm.metadata.api.service.PackageGenerateService;
import com.inspur.edp.lcm.metadata.api.service.ProjectExtendService;
import com.inspur.edp.lcm.metadata.api.service.RefCommonService;
import com.inspur.edp.lcm.metadata.core.GspProjectServiceImp;
import com.inspur.edp.lcm.metadata.core.MetadataProjectServiceImp;
import com.inspur.edp.lcm.metadata.core.MetadataServiceImp;
import com.inspur.edp.lcm.metadata.core.PackageGenerateServiceImp;
import com.inspur.edp.lcm.metadata.core.ProjectExtendServiceImp;
import com.inspur.edp.lcm.metadata.core.RefCommonServiceImp;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Classname ServiceConfiguration
 * @Description 配置类
 * @Date 2019/7/26 14:55
 * @Created by liu_bintr
 * @Version 1.0
 */
//todo
//@Configuration(proxyBeanMethods = false)
@Configuration()
public class ServiceConfiguration {
    @Bean
    public MetadataService createMetadataService() {
        return new MetadataServiceImp();
    }

    @Bean
    public GspProjectService createGspProjectService() {
        return new GspProjectServiceImp();
    }

    @Bean
    public MetadataProjectService createMetadataProjectService() {
        return new MetadataProjectServiceImp();
    }

    @Bean
    public RefCommonService createRefService() {
        return new RefCommonServiceImp();
    }

    @Bean
    public PackageGenerateService createPackageGenerateService() {
        return new PackageGenerateServiceImp();
    }

    @Bean
    public ProjectExtendService createProjectExtendService() {
        return new ProjectExtendServiceImp();
    }
}
