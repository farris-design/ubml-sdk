/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.core.event;

import com.inspur.edp.lcm.metadata.spi.event.MetadataCreateEventListener;
import io.iec.edp.caf.commons.event.CAFEventArgs;
import io.iec.edp.caf.commons.event.EventManager;
import io.iec.edp.caf.commons.event.IEventListener;

public class MetadataCreateEventManager extends EventManager {

    enum MetadataCreateEventType {
        metadataCreatingEvent,
        metadataCreatedEvent
    }

    @Override
    public String getEventManagerName() {
        return "MetadataCreateEventManager";
    }

    @Override
    public boolean isHandlerListener(IEventListener iEventListener) {
        return iEventListener instanceof MetadataCreateEventListener;
    }

    @Override
    public void addListener(IEventListener iEventListener) {
        checkHandlerListener(iEventListener);
        MetadataCreateEventListener metadataCreateEventListener = (MetadataCreateEventListener) iEventListener;
        this.addEventHandler(MetadataCreateEventType.metadataCreatingEvent, metadataCreateEventListener, "fireMetadataCreatingEvent");
        this.addEventHandler(MetadataCreateEventType.metadataCreatedEvent, metadataCreateEventListener, "fireMetadataCreatedEvent");
    }

    @Override
    public void removeListener(IEventListener iEventListener) {
        checkHandlerListener(iEventListener);
        MetadataCreateEventListener metadataCreateEventListener = (MetadataCreateEventListener) iEventListener;
        this.removeEventHandler(MetadataCreateEventType.metadataCreatingEvent, metadataCreateEventListener, "fireMetadataCreatingEvent");
        this.removeEventHandler(MetadataCreateEventType.metadataCreatedEvent, metadataCreateEventListener, "fireMetadataCreatedEvent");
    }

    public void fireMetadataCreatingEvent(CAFEventArgs args) {
        this.fire(MetadataCreateEventType.metadataCreatingEvent, args);
    }

    public void fireMetadataCreatedEvent(CAFEventArgs args) {
        this.fire(MetadataCreateEventType.metadataCreatedEvent, args);
    }

    private void checkHandlerListener(IEventListener iEventListener) {
        if (!isHandlerListener(iEventListener)) {
            throw new RuntimeException("指定的监听者" + iEventListener.getClass().getName()
                + "没有实现" + MetadataCreateEventListener.class.getName() + "接口");
        }
    }
}
