/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.common.definition.cef.json.element;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import org.openatom.ubml.model.common.definition.cef.element.GspEnumValue;
import org.openatom.ubml.model.common.definition.cef.json.CefNames;
import org.openatom.ubml.model.common.definition.cef.json.SerializerUtils;

import static com.fasterxml.jackson.core.JsonToken.FIELD_NAME;

/**
 * The Json Parser Of EnumValue
 *
 * @ClassName: GspEnumValueDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class GspEnumValueDeserializer extends JsonDeserializer<GspEnumValue> {
    @Override
    public GspEnumValue deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) {
        return deserializeGspEnumValue(jsonParser);
    }

    public GspEnumValue deserializeGspEnumValue(JsonParser jsonParser) {
        GspEnumValue enumValue = new GspEnumValue();
        SerializerUtils.readStartObject(jsonParser);
        while (jsonParser.getCurrentToken() == FIELD_NAME) {
            String propName = SerializerUtils.readPropertyName(jsonParser);
            readPropertyValue(enumValue, propName, jsonParser);
        }
        SerializerUtils.readEndObject(jsonParser);
        return enumValue;
    }

    private void readPropertyValue(GspEnumValue enumValue, String propName, JsonParser jsonParser) {

        switch (propName) {
            case CefNames.NAME:
                enumValue.setName(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CefNames.VALUE:
                enumValue.setValue(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CefNames.I18N_RESOURCE_INFO_PREFIX:
                enumValue.setI18nResourceInfoPrefix(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case CefNames.IS_DEFAULT_ENUM:
                enumValue.setIsDefaultEnum(SerializerUtils.readPropertyValue_boolean(jsonParser));
                break;
            case CefNames.INDEX:
                enumValue.setIndex(SerializerUtils.readPropertyValue_Integer(jsonParser));
                break;
            case CefNames.STRING_INDEX:
                enumValue.setStringIndex(SerializerUtils.readPropertyValue_String(jsonParser));//String类型的索引
                break;
            default:
                throw new RuntimeException(String.format("CefFieldDeserializer未识别的属性名：%1$s", propName));
        }
    }
}
