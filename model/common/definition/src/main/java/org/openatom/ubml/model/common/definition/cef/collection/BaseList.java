/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.common.definition.cef.collection;

import java.util.ArrayList;
import java.util.function.Predicate;

/**
 * The BaseList Definition,All List Use This Class
 *
 * @ClassName: CommonFieldRuleNames
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class BaseList<T> extends java.util.ArrayList<T> implements java.util.List<T>, Cloneable {

    private static final long serialVersionUID = 1L;


    /**
     * 构造一个新的列表
     */
    public BaseList() {
    }

    /**
     * 获取、设置指定位置的项目
     */
    public T getItem(int index) {
        if (index > -1 && index < this.getCount()) {
            return super.get(index);
        }
        return null;
    }

    public ArrayList<T> getAllItems(Predicate<T> predicate) {
        ArrayList<T> result = new ArrayList<T>();
        for (T item : this) {
            if (predicate.test(item)) {
                result.add(item);
            }
        }
        return result;
    }

    public T getItem(Predicate<T> predicate) {
        ArrayList<T> result = getAllItems(predicate);
        if (result.size() > 0) {
            return result.get(0);
        }
        return null;
    }

    public void setItem(int index, T value) {
        set(index, value);
    }

    /**
     * 移出指定位置的项目
     *
     * @param index
     */
    public void removeAt(int index) {
        super.remove(index);
    }

    /**
     * 在指定的位置插入项目
     *
     * @param index 插入位置
     * @param value 新项目
     */
    public void insert(int index, T value) {
        super.add(index, value);
    }

    /**
     * 移除指定的项目
     *
     * @param value
     */
    public void removeItem(Object value) {
        for (int i = 0; i < super.size(); i++) {
            if (super.get(i) == value) {
                super.remove(i);
                break;
            }
        }
    }

    /**
     * 是否包含指定的项目
     *
     * @param value
     * @return
     */
    @Override
    public boolean contains(Object value) {
        return super.contains(value);
    }


    // ICollection 成员
    public final boolean getIsSynchronized() {
        // TODO: 添加 BaseList.IsSynchronized getter 实现
        return false;
    }

    public int getCount() {
        return super.size();
    }

    @Override
    @SuppressWarnings("unchecked")
    public BaseList<T> clone() {
        return (BaseList<T>)super.clone();
    }
}