/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.common.definition.cef.json.operation;

import com.fasterxml.jackson.core.JsonParser;
import java.util.ArrayList;
import org.openatom.ubml.model.common.definition.cef.collection.DtmElementCollection;
import org.openatom.ubml.model.common.definition.cef.json.CefNames;
import org.openatom.ubml.model.common.definition.cef.json.SerializerUtils;
import org.openatom.ubml.model.common.definition.cef.operation.CommonDetermination;
import org.openatom.ubml.model.common.definition.cef.operation.CommonOperation;

/**
 * The Json Parser Of CommonDetermination
 *
 * @ClassName: CommonDtmDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class CommonDtmDeserializer extends CommonOpDeserializer {
    @Override
    protected boolean readExtendOpProperty(CommonOperation op, String propName, JsonParser jsonParser) {
        boolean result = true;
        CommonDetermination dtm = (CommonDetermination)op;
        switch (propName) {
            case CefNames.GET_EXECUTING_DATA_STATUS:
                dtm.setGetExecutingDataStatus(readGetExecutingDataStatus(jsonParser));
                break;
            case CefNames.REQUEST_ELEMENTS:
                dtm.setRequestElements(readRequestElements(jsonParser));
                break;
            default:
                result = false;
                break;
        }
        return result;
    }

    private DtmElementCollection readRequestElements(JsonParser jsonParser) {
        ArrayList<String> list = SerializerUtils.readStringArray(jsonParser);
        DtmElementCollection collection = new DtmElementCollection();
        for (int i = 0; i < list.size(); i++) {
            collection.add(list.get(i));
        }
        return collection;
    }

    @Override
    protected CommonOperation CreateCommonOp() {
        return new CommonDetermination();
    }
}
