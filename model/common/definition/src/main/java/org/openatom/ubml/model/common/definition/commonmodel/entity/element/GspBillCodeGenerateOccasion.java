/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.common.definition.commonmodel.entity.element;

/**
 * The Definition Of Element Code Rule Generator Occasion
 *
 * @ClassName: GspBillCodeGenerateOccasion
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public enum GspBillCodeGenerateOccasion {
    /**
     * 系统定义
     */
    SystemProcess(0),

    /**
     * 创建时
     */
    CreatingTime(1),

    /**
     * 保存时
     */
    SavingTime(2),

    /**
     * 创建和保存时
     */
    BothCreatingAndSaving(3);

    private static java.util.HashMap<Integer, GspBillCodeGenerateOccasion> mappings;
    private int intValue;

    private GspBillCodeGenerateOccasion(int value) {
        intValue = value;
        GspBillCodeGenerateOccasion.getMappings().put(value, this);
    }

    private synchronized static java.util.HashMap<Integer, GspBillCodeGenerateOccasion> getMappings() {
        if (mappings == null) {
            mappings = new java.util.HashMap<Integer, GspBillCodeGenerateOccasion>();
        }
        return mappings;
    }

    public static GspBillCodeGenerateOccasion forValue(int value) {
        return getMappings().get(value);
    }

    public int getValue() {
        return intValue;
    }
}