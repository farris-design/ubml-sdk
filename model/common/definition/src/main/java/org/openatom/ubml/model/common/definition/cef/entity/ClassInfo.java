/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.common.definition.cef.entity;

/**
 * The Definition Of ClassInfo
 *
 * @ClassName: ClassInfo
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ClassInfo {
    private DataTypeAssemblyInfo assemblyInfo;
    private String className = "";
    private String classNamespace = "";
    public ClassInfo(DataTypeAssemblyInfo assemblyInfo, String className, String classNamespace) {
        this.className = className;
        this.classNamespace = classNamespace;
        this.assemblyInfo = assemblyInfo;
    }

    public final DataTypeAssemblyInfo getAssemblyInfo() {
        return assemblyInfo;
    }

    public final String getClassName() {
        return className;
    }

    public final String getClassNamespace() {
        return classNamespace;
    }

    public final String getClassFullName() {
        return String.format("%1$s.%2$s", getClassNamespace(), getClassName());
    }
}