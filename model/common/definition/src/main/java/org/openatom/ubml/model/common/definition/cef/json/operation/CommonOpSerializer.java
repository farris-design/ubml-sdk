/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.common.definition.cef.json.operation;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import java.util.EnumSet;
import org.openatom.ubml.model.common.definition.cef.json.CefNames;
import org.openatom.ubml.model.common.definition.cef.json.SerializerUtils;
import org.openatom.ubml.model.common.definition.cef.operation.CommonOperation;
import org.openatom.ubml.model.common.definition.cef.operation.ExecutingDataStatus;

/**
 * The Json Serializer Of Common Operation
 *
 * @ClassName: CommonOpSerializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public abstract class CommonOpSerializer<T extends CommonOperation> extends JsonSerializer<T> {

    @Override
    public void serialize(T value, JsonGenerator gen, SerializerProvider serializers) {
        SerializerUtils.writeStartObject(gen);
        writeBaseProperty(value, gen);
        writeSelfProperty(value, gen);
        SerializerUtils.writeEndObject(gen);
    }

    private void writeBaseProperty(CommonOperation info, JsonGenerator gen) {
        SerializerUtils.writePropertyValue(gen, "ID", info.getID());
        this.writeExtendCommonOpBaseProperty(gen, info);
    }


    private void writeSelfProperty(CommonOperation info, JsonGenerator writer) {
        SerializerUtils.writePropertyValue(writer, CefNames.CODE, info.getCode());
        SerializerUtils.writePropertyValue(writer, CefNames.NAME, info.getName());
        SerializerUtils.writePropertyValue(writer, CefNames.DESCRIPTION, info.getDescription());
        SerializerUtils.writePropertyValue(writer, CefNames.COMPONENT_ID, info.getComponentId());
        SerializerUtils.writePropertyValue(writer, CefNames.COMPONENT_NAME, info.getComponentName());
        SerializerUtils.writePropertyValue(writer, CefNames.COMPONENT_PKG_NAME, info.getComponentPkgName());
        SerializerUtils.writePropertyValue(writer, CefNames.IS_REF, info.getIsRef());
        SerializerUtils.writePropertyValue(writer, CefNames.IS_GENERATE_COMPONENT, info.getIsGenerateComponent());
        this.writeExtendCommonOpSelfProperty(writer, info);

    }

    protected abstract void writeExtendCommonOpBaseProperty(JsonGenerator writer, CommonOperation info);

    protected abstract void writeExtendCommonOpSelfProperty(JsonGenerator writer, CommonOperation info);

    protected void writeGetExecutingDataStatus(JsonGenerator writer, EnumSet<ExecutingDataStatus> value) {
        int intValue = 0;
        for (ExecutingDataStatus timePointType : value) {
            intValue += timePointType.getValue();
        }
        SerializerUtils.writePropertyValue_Integer(writer, intValue);
    }
}
